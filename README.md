# PWA Contact Tracer

COVID19 Contact Tracer PWA.
Based on the poll result, we will use ReactJS for this

### Tech

* [React JS](https://reactjs.org/) - A JavaScript library for building user interfaces
* [Gatsby JS](https://www.gatsbyjs.org/) - Gatsby is a free and open source framework based on React that helps developers build blazing fast websites and apps
* [Material UI](https://material-ui.com/) - maybe? 

### Installation

Install the dependencies and devDependencies and start the server.

```sh
$ npm run install
$ npm run build:static
$ npm run start
```
### Development

Optimize images from /src/staticSrc and place it in /static
```sh
$ npm run build:images
```

Watch files in src/staticSrc and automatically optimize and place it in /static
```sh
$ npm run watch:static
```
