import React from "react"

import SEO from "../components/seo"

import Helmet from "react-helmet"
import { withPrefix } from "gatsby"

import "../scss/index.scss"

const IndexPage = () => (
  <div>
    <SEO title="Home" />

    <div>
      <div
        className="bg-container-contact100"
        style={{ backgroundImage: "url(images/bg-01.jpg)" }}
      >
        <div className="contact100-header flex-sb-m">
          {/* <a href="#" className="contact100-header-logo">
            <img src="images/icons/logo.png" alt="LOGO" />
          </a> */}
          {/* <div>
            <button className="btn-show-contact100">Contact Us</button>
          </div> */}
        </div>
      </div>
      <div className="container-contact100">
        <div className="wrap-contact100">
          {/* <button className="btn-hide-contact100">
            <i className="zmdi zmdi-close" />
          </button> */}
          <div
            className="contact100-form-title"
            style={{ backgroundImage: "url(images/bg-02.jpg)" }}
          >
            <span>DevCon Contact Tracing</span>
          </div>
          <form className="contact100-form validate-form">
            <div className="wrap-input100 validate-input">
              <input
                id="name"
                className="input100"
                type="text"
                name="name"
                placeholder="Full name"
              />
              <span className="focus-input100" />
              <label className="label-input100" htmlFor="name">
                <span className="lnr lnr-user m-b-2" />
              </label>
            </div>
            <div className="wrap-input100 validate-input">
              <input
                id="email"
                className="input100"
                type="text"
                name="email"
                placeholder="Eg. Location"
              />
              <span className="focus-input100" />
              <label className="label-input100" htmlFor="email">
                <span className="lnr lnr-location m-b-5" />
              </label>
            </div>
            <div className="wrap-input100 validate-input">
              <input
                id="phone"
                className="input100"
                type="text"
                name="phone"
                placeholder="Eg. +1 800 000000"
              />
              <span className="focus-input100" />
              <label className="label-input100" htmlFor="phone">
                <span className="lnr lnr-smartphone m-b-2" />
              </label>
            </div>
            <div className="wrap-input100 validate-input">
              <textarea
                id="message"
                className="input100"
                name="message"
                placeholder="Your notes, comments, description..."
                defaultValue={""}
              />
              <span className="focus-input100" />
              <label className="label-input100 rs1" htmlFor="message">
                <span className="lnr lnr-bubble" />
              </label>
            </div>
            <div className="container-contact100-form-btn">
              <button className="contact100-form-btn">Send Now</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    <Helmet>
      <script
        async
        defer
        src={withPrefix("js/compressed.js")}
        type="text/javascript"
      />
    </Helmet>
  </div>
)

export default IndexPage
